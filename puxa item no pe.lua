gpPushEnabled = false
gpPushDelay = 1 -- safe value: 1ms

local tab = setDefaultTab("Main")
local switch = addSwitch("gpPushEnabled", "Enable Anti Push", function(widget)
    widget:setOn(not widget:isOn())
    gpPushEnabled = widget:isOn()
end, tab)

macro(gpPushDelay, function ()
    if gpPushEnabled then
        push(0, -1, 0)
        push(0, 1, 0)
        push(-1, -1, 0)
        push(-1, 0, 0)
        push(-1, 1, 0)
        push(1, -1, 0)
        push(1, 0, 0)
        push(1, 1, 0)
    end
end)

function push(x, y, z)
    local position = player:getPosition()
    position.x = position.x + x
    position.y = position.y + y
  
    local tile = g_map.getTile(position)
    local thing = tile:getTopThing()
    if thing and thing:isItem() then
      g_game.move(thing, player:getPosition(), thing:getCount())
    end
end












