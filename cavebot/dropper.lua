CaveBot.Extensions.Dropper = {}

local isSendMensage = false

-- first function called, here you should setup your UI
CaveBot.Extensions.Dropper.setup = function()  
  CaveBot.registerAction("DropperReis", "#00FFFF", function(posicao, retries)
    if isSendMensage == false then
      warn("Ola, preencha o modulo ItemsDropperReis para sabermos quais itens iremos dropar para voce")
      isSendMensage = true
    end

    local tabelaDados = regexMatch(posicao, "\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*,\\s*([0-9]+)")
    local tabelaItems = storage["dropItems"]

    for i, container in pairs(getContainers()) do
      for j, items in ipairs(container:getItems()) do
        for k, itemsParaDropar in ipairs(tabelaItems) do
          local itemIteradoParaDropar = itemsParaDropar["id"]
          local idItemIterado = items:getId();
          if idItemIterado == itemIteradoParaDropar then
            if itemIteradoParaDropar ~= nil then
              if items:isStackable() then
                g_game.move(items, player:getPosition(), items:getCount())
                return true
              else
                g_game.move(items, player:getPosition(), dropItem.count)
                return true
              end
              return true
            end
            return true
          end
        end
      end
    end
    

    if retries >= 5 then
      print("CaveBot[DropperReis]: too many tries, can't drop item")
      return false -- tried 5 times, can't open
    end
    return true
  end)

  
  CaveBot.Editor.registerAction("dropperreis", "Dropper Reis", {
    value=function() return posx() .. "," .. posy() .. "," .. posz() end,
    title="DropperReis",
    description="Posicao onde os itens serao dropados (x,y,z)",
    multiline=false,    
    validation="^\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*,\\s*([0-9]+)$"
  })
end


