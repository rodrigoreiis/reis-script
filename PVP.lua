-- tools tab
--setDefaultTab("Reis")

--COMBOS
--[[
UI.Separator()
macro(100, "Combos", "End",  function()
    if not g_game.isAttacking() then
        return
    end
    if g_game.isAttacking() then
        say(storage.Spell1, 1)
        say(storage.Spell2, 1)
        say(storage.Spell3, 1)
        say(storage.Spell4, 1)
        say(storage.Spell5, 1)
    end
end)
addTextEdit("Spell1", storage.Spell1 or "Digite a Magia 1", function(widget, text) 
storage.Spell1 = text
end)
addTextEdit("Spell2", storage.Spell2 or "Digite a Magia 2", function(widget, text) 
storage.Spell2 = text
end)
addTextEdit("Spell3", storage.Spell3 or "Digite a Magia 3", function(widget, text) 
storage.Spell3 = text
end)
addTextEdit("Spell4", storage.Spell4 or "Digite a Magia 4", function(widget, text) 
storage.Spell4 = text
end)
addTextEdit("Spell5", storage.Spell5 or "Digite a Magia 5", function(widget, text) 
storage.Spell5 = text
end)
UI.Separator()


--Buff
UI.Separator()
UI.Label("Magias Especiais")
macro(500, "Buff's", function()
if hasPartyBuff() then return end
  if TargetBot then 
    TargetBot.saySpell(storage.buffSpell) -- sync spell with targetbot if available
  else
    say(storage.buffSpell, 1)
  end
end)
UI.TextEdit(storage.buffSpell or "Buff", function(widget, newText)
  storage.buffSpell = newText
end)

--Reflect
macro(500, "Reflect", function() 

    if not g_game.isAttacking() then
        return
    end

  if TargetBot then 
    TargetBot.saySpell(storage.reflectSpell) -- sync spell with targetbot if available
  else
    say(storage.reflectSpell, 1)
  end
end)
UI.TextEdit(storage.reflectSpell or "reflect", function(widget, newText)
  storage.reflectSpell = newText
end)

--Imortal
macro(500, "Imortal", function() 
  if TargetBot then 
    TargetBot.saySpell(storage.imortalSpell) -- sync spell with targetbot if available
  else
    say(storage.imortalSpell, 1)
  end
end)
UI.TextEdit(storage.imortalSpell or "Imortal", function(widget, newText)
  storage.imortalSpell = newText
end)

--Imortal
macro(500, "Trap's", function() 

    if not g_game.isAttacking() then
        return
    end

  if TargetBot then 
    TargetBot.saySpell(storage.trapSpell) -- sync spell with targetbot if available
  else
    say(storage.trapSpell, 1)
  end
end)
UI.TextEdit(storage.trapSpell or "Trap", function(widget, newText)
  storage.trapSpell = newText
end)

--Summon
macro(500, "Summon's", function() 
  if TargetBot then 
    TargetBot.saySpell(storage.summonSpell) -- sync spell with targetbot if available
  else
    say(storage.summonSpell, 1)
  end
end)
UI.TextEdit(storage.summonSpell or "Summons", function(widget, newText)
  storage.summonSpell = newText
end)
UI.Separator()

--]]