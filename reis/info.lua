setDefaultTab("Reis")

local panelName = "painelInformacoes"
local ui = setupUI([[
Panel
  height: 19

  Button
    id: stateInfo
    anchors.top: prev.top
    anchors.left: prev.right
    anchors.right: parent.right
    margin-left: 3
    height: 17
    width: 160
    text: Painel de Informacoes
]])
ui:setId(panelName)


if not storage[panelName] then
  storage[panelName] = {
    enabled = true,
  }
end

ui.stateInfo.onClick = function(widget)
  InfoPanel:show()
end


rootWidget = g_ui.getRootWidget()
InfoPanel = g_ui.createWidget('InfoPanel', rootWidget)
InfoPanel:hide()

InfoPanel.closeButton.onClick = function()
  InfoPanel:hide()
end

local function verificaMinSharedExp()
  local playerLevel = player:getLevel()
  InfoPanel.expPanel.leftLabelExpValue:setText((playerLevel / 3) * 2)
end

local function verificaMaxSharedExp()
  local playerLevel = player:getLevel()
  InfoPanel.expPanel.leftLabelExpValue2:setText(playerLevel * 1.5)
end

local function getPlayerMagicLevel() 
  --skillId 0 fist, 1 club e assim por diante
end

-- HORA MINUTO SEGUNDO DO PC
--warn(os.date('%H:%M:%S'))

macro(1000, function ()
  verificaMinSharedExp()
  verificaMaxSharedExp() 
end)

addSeparator()
