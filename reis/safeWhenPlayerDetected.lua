setDefaultTab("Reis")

local triesCounter = 0
local panelName = "safeWhenPlayerDetected"
local ui = setupUI([[
Panel
  height: 19

  BotSwitch
    id: title
    anchors.top: parent.top
    anchors.left: parent.left
    text-align: center
    width: 130
    !text: tr('Entrar safe')

  Button
    id: config
    anchors.top: prev.top
    anchors.left: prev.right
    anchors.right: parent.right
    margin-left: 3
    height: 17
    text: Setup

]])
ui:setId(panelName)


if not storage[panelName] then
    storage[panelName] = {
      enabled = true,
      positionList = {},
      friendList = {},
      tries = 0
    }
end

ui.title:setOn(storage[panelName].enabled)
ui.title.onClick = function(widget)
    storage[panelName].enabled = not storage[panelName].enabled
    widget:setOn(storage[panelName].enabled)
end

ui.config.onClick = function()
    Safe:show()
end

rootWidget = g_ui.getRootWidget()
Safe = g_ui.createWidget('SafeWhenPlayerDetected', rootWidget)
Safe:hide()

Safe.closeButton.onClick = function(widget)
    Safe:hide()
end

if storage[panelName].friendList and #storage[panelName].friendList > 0 then
  for index, amigo in pairs(storage[panelName].friendList) do
    local label = g_ui.createWidget("PlayerName", Safe.FriendList)
    label.remove.onClick = function(widget)
      table.removevalue(storage[panelName].friendList, label:getText())
      label:destroy()
    end
    label:setText(amigo)
  end
end

if storage[panelName].positionList and #storage[panelName].positionList > 0 then
  for index, amigo in pairs(storage[panelName].positionList) do
    local label = g_ui.createWidget("PlayerName", Safe.PositionList)
    label.remove.onClick = function(widget)
      table.removevalue(storage[panelName].positionList, label:getText())
      label:destroy()
    end
    label:setText(amigo)
  end
end

Safe.AddFriend.onClick = function(widget)
    local friendName = Safe.FriendName:getText()
    if friendName:len() > 0 and not table.contains(storage[panelName].friendList, friendName, true) then
        table.insert(storage[panelName].friendList, friendName)
        local label = g_ui.createWidget("PlayerName", Safe.FriendList)
        label.remove.onClick = function()
          table.removevalue(storage[panelName].friendList, label:getText())
          label:destroy()
        end
        label:setText(friendName)
        Safe.FriendName:setText('')
    end
end

Safe.AddPosition.onClick = function(widget)
  local pos = player:getPosition()
  local posString = tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z)
  if posString.len() > 0 and not table.concat(storage[panelName].positionList, posString, true) then
    table.insert(storage[panelName].positionList, posString);
    local label = g_ui.createWidget("PlayerName", Safe.PositionList)
    label.remove.onClick = function(widget)
      table.removevalue(storage[panelName].positionList, posString)
      label:destroy()
    end 
  end
  label:setText(posString)
end


playerName = g_ui.createWidget('PlayerName', rootWidget);
playerName:hide();

playerName.remove.onClick = function(widget)
    if storage[panelName].friendList and storage[panelName].friendList > 0 then
        for _, name in ipairs(storage[panelName].friendList) do
            local label = g_ui.createWidget("PlayerName", Safe.FriendList)
            label.remove.onClick = function(widget)
              table.removevalue(storage[panelName].friendList, label:getText())
              label:destroy()
            end
            label:setText(name)
          end
    end
end


local macroTimer = 200;
warn(storage[panelName].tries)

if storage[panelName].tries and storage[panelName].tries > 3 then 
   storage[panelName].tries = 0
   macroTimer = 10000
end

macro(macroTimer,  function(macro)  
  if storage[panelName].enabled then
      local specs = getSpectators()
      for index, spec in ipairs(specs) do
        local amigos = storage[panelName].friendList
        if not spec:isLocalPlayer() then
          if spec:isPlayer() then
            if not table.contains(storage[panelName].friendList, spec:getName()) then              
              if storage[panelName].tries then
                 storage[panelName].tries = storage[panelName].tries + 1
              end
              g_game.safeLogout()
            end
          end  
        end
      end      
    macroTimer = 200
  end
end)
addSeparator()